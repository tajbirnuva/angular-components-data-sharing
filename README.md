# Passing Value Between Components

This project was generated with version 14.2.6. In this project we can learn how to pass value between components.

## Parent To Child Component

- Declare a variable with `@Input()` decorator.
```typescript
import { Component, Input, OnInit } from '@angular/core';

export class ChildComponent implements OnInit {
  
  @Input() getMessage = '';

}
```

- Send Data in Chlid component's Route tag.
```html
 <app-child [getMessage]="message"></app-child>
```

## Child To Parent Component

- Declare an EventEmitter reference variable with `@Output()` decorator according to your desire data type.
- Declare your data variable in which you store you value.
- Emit your data variable into EventEmitter reference variable.
```typescript
import { Component, EventEmitter, OnInit, Output } from '@angular/core';

export class ChildComponent implements OnInit {
  @Output() event = new EventEmitter<string>();

  message = '';

  sendToParent() {
    this.event.emit(this.message);
  }
}
```

- In Parent component declare a event method to get Chlid component's data.
```typescript
import { Component, OnInit } from '@angular/core';

export class ParentComponent implements OnInit {
  getMessage = '';

  receiveFromChild($event: any) {
    this.getMessage = $event;
  }
}
```

- Call Parent component's event method from Chlid component's Route tag.
```html
 <app-child (event)="receiveFromChild($event)"></app-child>
```