import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-parent-receive',
  templateUrl: './parent-receive.component.html',
  styleUrls: ['./parent-receive.component.scss'],
})
export class ParentReceiveComponent implements OnInit {
  getMessage = '';

  constructor() {}

  ngOnInit(): void {}

  receiveFromChild($event: any) {
    this.getMessage = $event;
  }
}
