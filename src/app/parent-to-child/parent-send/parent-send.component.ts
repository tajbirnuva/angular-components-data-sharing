import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-parent-send',
  templateUrl: './parent-send.component.html',
  styleUrls: ['./parent-send.component.scss'],
})
export class ParentSendComponent implements OnInit {
  message='';

  constructor() {}

  ngOnInit(): void {}
}
