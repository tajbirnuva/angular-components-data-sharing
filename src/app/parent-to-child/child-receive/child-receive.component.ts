import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-child-receive',
  templateUrl: './child-receive.component.html',
  styleUrls: ['./child-receive.component.scss'],
})
export class ChildReceiveComponent implements OnInit {
  @Input() getMessage = '';
  
  constructor() {}

  ngOnInit(): void {}
}
