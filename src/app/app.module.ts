import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ParentReceiveComponent } from './child-to-parent/parent-receive/parent-receive.component';
import { ChildSendComponent } from './child-to-parent/child-send/child-send.component';
import { ParentSendComponent } from './parent-to-child/parent-send/parent-send.component';
import { ChildReceiveComponent } from './parent-to-child/child-receive/child-receive.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { MatDividerModule } from '@angular/material/divider';
import { MatCardModule } from '@angular/material/card';
@NgModule({
  declarations: [
    AppComponent,
    ParentReceiveComponent,
    ChildSendComponent,
    ParentSendComponent,
    ChildReceiveComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatInputModule,
    FormsModule,
    MatDividerModule,
    MatCardModule,
    NgbModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
