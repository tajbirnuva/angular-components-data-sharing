import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-child-send',
  templateUrl: './child-send.component.html',
  styleUrls: ['./child-send.component.scss'],
})
export class ChildSendComponent implements OnInit {
  @Output() event = new EventEmitter<string>();

  message = '';

  constructor() {}

  ngOnInit(): void {}

  sendToParent() {
    this.event.emit(this.message);
  }
}
